package main

import (
	"bitbucket.org/fanky5g/talentsinafrica/crawler/crawlers/businessghana.com"
	"bitbucket.org/fanky5g/talentsinafrica/crawler/crawlers/ghanacurrentjobs.com"
	"bitbucket.org/fanky5g/talentsinafrica/crawler/crawlers/jobberman.com.gh"
	"bitbucket.org/fanky5g/talentsinafrica/crawler/crawlers/jobhouse.com.gh"
	"bitbucket.org/fanky5g/talentsinafrica/crawler/crawlers/joblistghana.com"
	"bitbucket.org/fanky5g/talentsinafrica/crawler/crawlers/jobwebghana.com"
	"bitbucket.org/fanky5g/talentsinafrica/crawler/log"
	"time"
)

func main() {
	log.Info("Crawler init\n")
	log.Infof("Start time: %s", time.Now().UTC().String())
	log.Info("Fetching jobs from jobberman.com.gh")

	result, err := jobberman.DoCrawl()
	if err != nil {
		log.Debugf("Jobberman crawl returned error: %s", err)
	}

	log.Infof("Jobberman crawl result: %s", result)
	log.Info("Fetching jobs from businessghana.com")

	result, err = businessghana.DoCrawl()
	if err != nil {
		log.Debugf("Businessghana crawl returned error: %s", err)
	}

	log.Infof("Businessghana crawl result: %s", result)
	log.Info("Fetching jobs from ghanacurrentjobs.com")

	result, err = ghanacurrentjobs.DoCrawl()
	if err != nil {
		log.Debugf("Ghanacurrentjobs crawl returned error: %s", err)
	}

	log.Infof("Ghanacurrentjobs crawl result: %s", result)
	log.Info("Fetching jobs from jobhouse.com.gh")

	result, err = jobhouse.DoCrawl()
	if err != nil {
		log.Debugf("Jobhouse crawl returned error: %s", err)
	}

	log.Infof("Jobhouse crawl result: %s", result)
	log.Info("Fetching jobs from joblistghana.com")

	result, err = joblistghana.DoCrawl()
	if err != nil {
		log.Debugf("Joblistghana crawl returned error: %s", err)
	}

	log.Infof("Joblistghana crawl result: %s", result)
	log.Info("Fetching jobs from jobwebghana.com")

	result, err = jobwebghana.DoCrawl()
	if err != nil {
		log.Debugf("Jobwebghana crawl returned error: %s", err)
	}
	log.Infof("Jobwebghana crawl result: %s", result)

	log.Info("Crawling complete")
	log.Infof("End time: %s", time.Now().UTC().String())
}
