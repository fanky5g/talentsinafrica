package handlers

import (
	"errors"

	appConfig "bitbucket.org/fanky5g/talentsinafrica/web/server/config"
	"bitbucket.org/fanky5g/talentsinafrica/web/server/log2"
	"bitbucket.org/fanky5g/talentsinafrica/web/server/upload"
	"github.com/olebedev/config"
)

// Handlers holds definitions for api controllers
type Handlers struct {
	Conf     *config.Config
	S3Client *upload.S3Agent
}

var (
	cfg, _ = appConfig.GetConfig()
	logger = log2.Log
	// APPRoot defines application url
	APPRoot          = cfg.AppURL
	errBadParameters = errors.New("bad form parameters sent")
	errAlreadyExists = errors.New("already exists")
)
