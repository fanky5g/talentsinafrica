package lib

/*
// http://www.scientiamobile.com/docs/c-api-user-guide.html/
#cgo LDFLAGS: -lwurfl -L/usr/local/lib

#include <stdlib.h>
#include <libwurfl/src/wurfl.h>
*/
// import "C"

// import (
// 	"errors"
// 	"strconv"
// 	"sync"
// 	"unsafe"
// )

// type Wurfl struct {
// 	wurfl C.wurfl_t
// 	mu    sync.Mutex
// }

// type Device struct {
// 	Device       string                 `json:"device"`
// 	Capabilities map[string]interface{} `json:"capabilities"`
// 	// VirtualCapabilities map[string]interface{} `json:"virtual"`
// }

// func New(wurflxml string, patches ...string) (*Wurfl, error) {
// 	w := &Wurfl{}
// 	wxml := C.CString(wurflxml)

// 	var freeme []*C.char
// 	defer func() {
// 		C.free(unsafe.Pointer(wxml))
// 		for _, f := range freeme {
// 			C.free(unsafe.Pointer(f))
// 		}
// 	}()

// 	for _, pxml := range patches {
// 		p := C.CString(pxml)
// 		freeme = append(freeme, p)
// 	}

// 	w.wurfl = C.wurfl_init(wxml, freeme)

// 	return w, nil
// }

// func concreteProperty(val string) interface{} {

// 	if val == "true" || val == "false" {
// 		return val == "true"
// 	}

// 	// check for numbers
// 	n, err := strconv.Atoi(val)
// 	if err == nil {
// 		return n
// 	}

// 	return val
// }

// func (w *Wurfl) LookupProperties(useragent string, proplist []string, vproplist []string) *Device {

// 	w.mu.Lock()
// 	defer w.mu.Unlock()

// 	ua := C.CString(useragent)

// 	device := C.wurfl_match(w.wurfl, ua)
// 	C.free(unsafe.Pointer(ua))

// 	if device == nil {
// 		return nil
// 	}

// 	m := make(map[string]interface{})

// 	for _, prop := range proplist {
// 		cprop := C.CString(prop)
// 		val := C.device_capability(device, cprop)
// 		C.free(unsafe.Pointer(cprop))
// 		m[prop] = concreteProperty(C.GoString(val))
// 	}

// 	// get the virtual properties
// 	// mv := make(map[string]interface{})
// 	// for _, prop := range vproplist {
// 	// 	cprop := C.CString(prop)
// 	// 	val := C.wurfl_device_get_virtual_capability(device, cprop)
// 	// 	C.free(unsafe.Pointer(cprop))
// 	// 	mv[prop] = concreteProperty(C.GoString(val))
// 	// }

// 	d := &Device{
// 		Device:       C.GoString(C.wurfl_device_get_id(device)),
// 		Capabilities: m,
// 		// VirtualCapabilities: mv,
// 	}
// 	C.wurfl_device_destroy(device)

// 	return d
// }

// func wurfl_capability_enumerate(enumerator C.char) map[string]interface{} {

// 	m := make(map[string]interface{})

// 	for C.wurfl_device_capability_enumerator_is_valid(enumerator) != 0 {
// 		// name := C.wurfl_device_capability_enumerator_get_name(enumerator)
// 		// val := C.wurfl_device_capability_enumerator_get_value(enumerator)

// 		if name != nil && val != nil {
// 			gname := C.GoString("name")
// 			gval := C.GoString("val")
// 			m[gname] = concreteProperty(gval)
// 		}

// 		C.wurfl_device_capability_enumerator_move_next(enumerator)
// 	}

// 	return m
// }

// func (w *Wurfl) Lookup(useragent string) *Device {

// 	w.mu.Lock()
// 	defer w.mu.Unlock()

// 	ua := C.CString(useragent)
// 	device := C.wurfl_lookup_useragent(w.wurfl, ua)
// 	C.free(unsafe.Pointer(ua))

// 	if device == nil {
// 		return nil
// 	}

// 	enumerator := C.device_capabilities(device, C.NULL)
// 	m := wurfl_capability_enumerate(unsafe.Pointer(enumerator))

// 	d := &Device{
// 		Device:       C.GoString(C.wurfl_device_get_id(device)),
// 		Capabilities: m,
// 	}
// 	return d
// }
