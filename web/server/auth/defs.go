package auth

import (
	"bitbucket.org/fanky5g/talentsinafrica/web/server/config"
)

var (
	cfg, _ = config.GetConfig()

	// APPRoot defines application url
	APPRoot = cfg.AppURL
)
