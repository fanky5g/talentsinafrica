package config

import (
	"bitbucket.org/fanky5g/talentsinafrica/web/server/types"
	"errors"
	"fmt"
	"github.com/olebedev/config"
	"reflect"
)

var confString = `
Port: 5000
AWSDefaultRegion: us-west-2
AWSAccessKeyID: AKIAJOM6I4IC7CYP7UFQ
AWSSecretKey: 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
AWSToken:
BucketRegion: us-east-1
ImageBucketRegion: us-west-2
Bucket: talentsinafrica_store
ImageBucket: talentscommunity-images
TemplateDir: server/data/templates
AppLogo: https://talentsinafrica.com/static/images/logo-web.png
AppURL: https://talentsinafrica.com
AppName: Talents Community
AppIcon: https://talentsinafrica.com/favicon.ico
DefaultAvatar: images/gravatar.png
Secret: Z29sZDU5
SMTPUser: support@talentsinafrica.com
SMTPPswd: U2FsdGVkX1+jKpK+z5HIGjNUQqF8ydh8ckb+qEt8s9tzxRt5jCypPiAze/b8uAZs
SMTPHost: smtp.zoho.com
SMTPPort: 587
DBName: talentsinafrica
DBUser: dbadmin
DBPass: delta5000
SMTPAddr: support@talentsinafrica.com
FileDB: files
NumWorkers: 100
IPayMerchantKey: eee8ba4a-aa5e-11e6-887a-f23c9170642f
GoogleAPIKey: 
certFile: /etc/letsencrypt/live/talentsinafrica.com/cert.pem
keyFile: /etc/letsencrypt/live/talentsinafrica.com/privkey.pem
chainFile: /etc/letsencrypt/live/talentsinafrica.com/chain.pem
BleveIndexPath: /home/bleve/jobs.bleve
BleveProfilePath: /home/bleve/profile.bleve
ParserPath: /home/ResumeParser/ResumeTransducer
WURFLPath: web/server/wurfl.xml
`

// IPayMerchantKey: aad17df0-8f10-11e7-b5b1-f23c9170642f
// dcd7b80e-5032-11e7-a922-f23c9170642f

// GetConfig returns Config
func GetConfig() (*types.Config, error) {
	conf, err := config.ParseYaml(confString)

	if err != nil {
		return nil, err
	}

	var cfg types.Config
	l := reflect.ValueOf(cfg).NumField()
	for i := 0; i < l; i++ {
		v := reflect.ValueOf(cfg)
		k := v.Field(i).Kind()

		if k != reflect.Ptr {
			t := v.Type()
			fieldname := t.Field(i).Name
			val := conf.UString(fieldname)
			setField(&cfg, fieldname, val)
		}
	}
	return &cfg, nil
}

// GetRawConfig returns default application configuration from confString
func GetRawConfig() (*config.Config, error) {
	return config.ParseYaml(confString)
}

func setField(obj interface{}, name string, value interface{}) error {
	structValue := reflect.ValueOf(obj).Elem()
	structFieldValue := structValue.FieldByName(name)

	if !structFieldValue.IsValid() {
		return fmt.Errorf("No such field: %s in obj", name)
	}

	if !structFieldValue.CanSet() {
		return fmt.Errorf("Cannot set %s field value", name)
	}

	structFieldType := structFieldValue.Type()
	val := reflect.ValueOf(value)
	if structFieldType != val.Type() {
		return errors.New("Provided value type didn't match obj field type")
	}

	structFieldValue.Set(val)
	return nil
}
